/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** structure for frame buffer
*/

#ifndef FRAMEBUFFER_H_
#define FRAMEBUFFER_H_

#include <SFML/Graphics.h>

/* Framebuffer structure */
struct framebuffer {
	unsigned int width;
	unsigned int height;
	sfUint8 *pixels;
};
typedef struct framebuffer framebuffer_t;

/* prototypes for CSFML basic*/
framebuffer_t *framebuffer_create(unsigned int, unsigned int);
void framebuffer_destroy(framebuffer_t*);
void my_put_pixel(framebuffer_t*, unsigned int, unsigned int, sfColor);
void draw_circle(framebuffer_t*, sfVector2i, int, sfColor);
void draw_ring(framebuffer_t*, sfVector2i, int, sfColor);
void draw_rectangle(framebuffer_t*, sfVector2i , sfVector2i, sfColor);

#endif /* FRAMEBUFFER_H_ */
