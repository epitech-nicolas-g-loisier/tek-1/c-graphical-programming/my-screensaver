##
## EPITECH PROJECT, 2017
## Makefile
## File description:
## Compiles program
##

SRC	=	$(SRC_DIR)/my_screensaver.c	\
		$(SRC_DIR)/display_help.c	\
		$(SRC_DIR)/display_id_animation.c	\
		$(SRC_DIR)/open_window.c	\
		$(SRC_DIR)/draw_shapes.c	\
		$(SRC_DIR)/display_random_lign.c	\
		$(SRC_DIR)/display_epilepsy.c	\
		$(SRC_DIR)/display_star.c	\
		$(SRC_DIR)/display_root.c	\
		$(SRC_DIR)/display_beat.c	\
		$(SRC_DIR)/display_ball.c	\
		$(SRC_DIR)/ball_function.c	\
		$(SRC_DIR)/display_sonar.c	\
		$(SRC_DIR)/display_fall.c	\
		$(SRC_DIR)/display_patern.c

SRC_DIR	=	$(realpath ./src)

DEST_A	=	$(realpath ./lib/my)

NAME	=	my_screensaver

OBJ	=	$(SRC:.c=.o)

CFLAGS	=	-Wall -Wextra	\
		-Iinclude	\

LFLAGS	=	-L$(DEST_A) -lmy -lcsfml-window -lcsfml-graphics -lcsfml-system

all:	$(NAME)

$(NAME):	$(OBJ)
	make -C lib/my
	gcc -o $(NAME) $(OBJ) $(LFLAGS)

clean:
	rm -f $(OBJ)
	make clean -C lib/my

fclean:	clean
	rm -f $(NAME)
	make fclean -C lib/my

re:	fclean all
