/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** display ring which grow
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"
#include "ring.h"

ring_t	*first_ring(int less)
{
	ring_t	*eye = malloc(sizeof(ring_t));

	eye->position = (sfVector2i){960, 540};
	eye->color = (sfColor){rand() % 100 + 155 + less, rand() % 100 + 155,
				rand() % 100 + 155 + less, 255};
	eye->rayon = 40 - less;
	eye->next = NULL;
	return (eye);
}

void	display_eye(framebuffer_t *fbuffer, ring_t *eye)
{
	draw_ring(fbuffer, eye->position, eye->rayon, eye->color);
	eye->rayon += eye->rayon / 20;
	eye->color.r -= 2;
	eye->color.g -= 2;
	eye->color.b -= 2;
}

void	draw_eye(framebuffer_t *fbuffer)
{
	int	i = 0;
	int	j = 0;
	sfVector2i cent_up = {960, -200};
	sfVector2i cent_les = {960, 1280};
	int	r = 1180;

	while (i != 1920){
		j = 0;
		while (j != 1080){
			if (((i - cent_up.x) * (i - cent_up.x)) +
				((j - cent_up.y) * (j - cent_up.y)) > r * r){
				my_put_pixel(fbuffer, i, j, sfBlack);
			} else if (((i - cent_les.x) * (i - cent_les.x)) +
				((j - cent_les.y) * (j - cent_les.y)) > r * r){
				my_put_pixel(fbuffer, i, j, sfBlack);
			}
			j++;
		}
		i++;
	}
}

void	eye(sfRenderWindow *window, sfTexture *texture, sfSprite *sprite,
		framebuffer_t *fbuffer)
{
	static	ring_t	*eye;
	static	ring_t	*eye2;

	if (eye == NULL){
		eye = first_ring(0);
		eye2 = first_ring(20);
	}
	display_eye(fbuffer, eye2);
	display_eye(fbuffer, eye);
	draw_circle(fbuffer, eye->position, 60, sfBlack);
	if (eye->rayon > 960){
		eye = first_ring(0);
	} else if (eye2->rayon > 960)
		eye2 = first_ring(0);
	draw_eye(fbuffer);
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

int	main_eye(void)
{
	void	(*function)(sfRenderWindow*, sfTexture*, sfSprite*,
				framebuffer_t*);

	function = eye;
	open_window(function);
	return 0;
}
