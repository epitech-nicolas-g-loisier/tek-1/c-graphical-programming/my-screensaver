/*
** EPITECH PROJECT, 2017
** My Screnn Saver
** File description:
** Draw multiple shapes
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"

void	draw_circle(framebuffer_t *fbuffer, sfVector2i center, int r,
			sfColor color)
{
	int	i = 0;
	int	j = 0;

	while (i != 1920){
		j = 0;
		while (j != 1080){
			if (((i - center.x) * (i - center.x)) +
			((j - center.y) * (j - center.y)) <= r * r){
				my_put_pixel(fbuffer, i, j, color);
				}
			j++;
		}
		i++;
	}
}

void	draw_ring(framebuffer_t *fbuffer, sfVector2i center, int r,
			sfColor color)
{
	int	i = 0;
	int	j = 0;
	int	tmp = 0;
	int	tmp_r = r - r * 10 / 100;

	while (i != 1920){
		j = 0;
		while (j != 1080){
			tmp = (i - center.x) * (i - center.x);
			tmp += (j - center.y) * (j - center.y);
			if (tmp <= r * r && tmp >= tmp_r * tmp_r){
				my_put_pixel(fbuffer, i, j, color);
				}
			j++;
		}
		i++;
	}
}

void	draw_rectangle(framebuffer_t *fbuffer, sfVector2i top_left,
			sfVector2i low_right, sfColor color)
{
	int	i = top_left.x;
	int	j = top_left.y;

	while (i != low_right.x){
		j = top_left.y;
		while (j != low_right.y){
			my_put_pixel(fbuffer, i, j, color);
			j++;
		}
		i++;
	}
}
