/*
** EPITECH PROJECT, 2017
** Lib: convertisseur
** File description:
** convert decimal to octal
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

char	*convert_to_octal(int nb)
{
	long	i = 0;
	char	*result = malloc(sizeof(char) * 20);

	while (nb != 0){
		result[i] = (nb % 8) + 48;
		nb = nb / 8;
		i++;
	}
	result[i] = 0;
	return (my_revstr(result));
}
