/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** open a window
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"

void	my_put_pixel(framebuffer_t *buffer, unsigned int x, unsigned int y,
			sfColor color)
{
	unsigned long	position = ((y * (buffer->width)) + x) * 4;

	buffer->pixels[position] = color.r;
	buffer->pixels[(position + 1)] = color.g;
	buffer->pixels[(position + 2)] = color.b;
	buffer->pixels[(position + 3)] = color.a;
}

void	framebuffer_destroy(framebuffer_t *framebuffer)
{
	free(framebuffer->pixels);
}

framebuffer_t	*framebuffer_create(unsigned int width, unsigned int heigth)
{
	static	framebuffer_t framebuffer;
	sfUint8	*pixels;
	unsigned int	counter = 0;

	pixels = malloc(width * heigth * 32 / 8);
	while (counter < width * heigth * 32 / 8) {
		pixels[counter] = 0;
		counter = counter + 1;
	}
	framebuffer.width = width;
	framebuffer.height = heigth;
	framebuffer.pixels = pixels;
	return (&framebuffer);
}

void	check_event(sfRenderWindow* window, sfEvent event)
{
	while (sfRenderWindow_pollEvent(window, &event)){
		if (event.type == sfEvtMouseButtonPressed){
			sfRenderWindow_close(window);
		}else if (event.type == sfEvtKeyPressed){
			sfRenderWindow_close(window);
		} if (event.type == sfEvtMouseMoved){
			sfRenderWindow_close(window);
		}
	}
}

int	open_window(void (*function)(sfRenderWindow*, sfTexture*, sfSprite*,
					framebuffer_t*))
{
	sfVideoMode	mode = {1920, 1080, 32};
	sfRenderWindow	*window = sfRenderWindow_create(mode, "screensaver",
					sfFullscreen, NULL);;
	sfTexture	*texture = sfTexture_create(1920, 1080);;
	sfSprite	*sprite = sfSprite_create();
	sfEvent		event;
	framebuffer_t	*fbuffer = framebuffer_create(1920, 1080);;

	sfSprite_setTexture(sprite, texture, sfTrue);
	sfRenderWindow_setFramerateLimit(window, 120);
	while (sfRenderWindow_isOpen(window)){
		check_event(window, event);
		(*function)(window, texture, sprite, fbuffer);
		sfRenderWindow_clear(window, sfBlack);
	}
	framebuffer_destroy(fbuffer);
	sfSprite_destroy(sprite);
	sfTexture_destroy(texture);
	sfRenderWindow_destroy(window);
	return 0;
}
