/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** Draw line
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"

void	draw_line_h(sfRenderWindow *window, sfTexture *texture,
			sfSprite *sprite, framebuffer_t *fbuffer)
{
	int	random = (rand() % 1070);
	int	width = random + (rand() % 100) + 1;
	int	i = 0;
	int	j = 0;
	sfUint8	red = rand() % 255;
	sfUint8	green = rand() % 255;
	sfUint8	blue = rand() % 255;
	sfColor	color = {red, green, blue, 255};

	while (i != 1920){
		j = random;
		while (j != width && j != 1080){
			my_put_pixel(fbuffer, i, j, color);
			j++;
		}
		i++;
	}
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

void	draw_line_v(sfRenderWindow *window, sfTexture *texture,
			sfSprite *sprite, framebuffer_t *fbuffer)
{
	int	random = (rand() % 1920);
	int	width = random + (rand() % 100) + 1;
	int	i = random;
	int	j = 0;
	sfUint8	red = rand() % 255;
	sfUint8	green = rand() % 255;
	sfUint8	blue = rand() % 255;
	sfColor	color = {red, green, blue, 255};

	while (i != width && i != 1920){
		j = 0;
		while (j != 1080){
			my_put_pixel(fbuffer, i, j, color);
			j++;
		}
		i++;
	}
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

void	draw_line(sfRenderWindow *window, sfTexture *texture,
			sfSprite *sprite, framebuffer_t *fbuffer)
{
	int	choose = rand() % 2;

	if (choose == 1)
		draw_line_v(window, texture, sprite, fbuffer);
	else
		draw_line_h(window, texture, sprite, fbuffer);
	sfRenderWindow_clear(window, sfBlack);
}

int	main_draw_line(void)
{
	void		(*function)(sfRenderWindow*, sfTexture*, sfSprite*,
					framebuffer_t*);

	function = draw_line;
	open_window(function);
	return 0;
}
