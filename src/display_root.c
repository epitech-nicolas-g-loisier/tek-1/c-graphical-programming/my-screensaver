/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** ball
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "my_screensaver.h"

void	draw_root(framebuffer_t *fbuffer, int rand_dep)
{
	static sfVector2i top_left = {0, 0};
	static sfVector2i low_right = {10, 10};
	sfColor color = {rand() % 255, rand() % 255, rand() % 255, 255};

	if (rand_dep == 0 && top_left.x <= 1900){
		top_left.x += 10;
		low_right.x += 10;
	} else if (rand_dep == 1 && top_left.y <= 1060){
		top_left.y += 10;
		low_right.y += 10;
	} else {
		if (rand_dep == 2 && top_left.x >= 10){
			top_left.x -= 10;
			low_right.x -= 10;
		} else if (rand_dep == 3 && top_left.y >= 10){
			top_left.y -= 10;
			low_right.y -= 10;
		}
	}
	draw_rectangle(fbuffer, top_left, low_right, color);
}

void	root(sfRenderWindow *window, sfTexture *texture, sfSprite *sprite,
		framebuffer_t *fbuffer)
{
	int	i = 0;
	static int	count = 0;
	int	rand_dep = rand() % 4;

	while (i < 10){
		rand_dep = rand() % 4;
		draw_root(fbuffer, rand_dep);
		draw_root(fbuffer, rand_dep);
		draw_root(fbuffer, rand_dep);
		i++;
		count++;
	}
	if (count == 9000){
		draw_rectangle(fbuffer, (sfVector2i){0, 0},
				(sfVector2i){1920, 1080}, sfBlack);
		count = 0;
	}
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

int	main_root(void)
{
	void	(*function)(sfRenderWindow*, sfTexture*, sfSprite*,
				framebuffer_t*);

	function = root;
	open_window(function);
	return 0;
}
