/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** structure for ball
*/

#ifndef BALL_H_
#define BALL_H_

#include <SFML/Graphics.h>

struct ball {
	sfVector2i position;
	sfVector2i speed;
	sfColor	color;
	unsigned int ray;
	struct ball *next;
};
typedef struct ball ball_t;

#endif

ball_t	*first_ball(void);
ball_t	*create_ball(ball_t*);
void	display_ball(framebuffer_t*, ball_t*);
void	reduce_color(ball_t*);
void	delete_ball(ball_t*);
