/*
** EPITECH PROJECT, 2017
** lib for my_printf
** File description:
** flags for convertion
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

int	bin_flag(va_list ap)
{
	int	nb = va_arg(ap, int);

	my_putstr(convert_to_binary(nb));
	va_end(ap);
	return (0);
}

int	oct_flag(va_list ap)
{
	int	nb = va_arg(ap, int);

	my_putstr(convert_to_octal(nb));
	va_end(ap);
	return (0);
}

int	dec_flag(va_list ap)
{
	int	nb = va_arg(ap, int);

	if (nb < 0){
		nb += 2;
		nb = 4294967294 + nb;
	}
	my_putstr(long_to_str(nb));
	va_end(ap);
	return (0);
}

int	hexa_low_flag(va_list ap)
{
	int	nb = va_arg(ap, int);

	my_putstr(convert_to_exa_low(nb));
	va_end(ap);
	return (0);
}

int	hexa_up_flag(va_list ap)
{
	int	nb = va_arg(ap, int);

	my_putstr(convert_to_exa_up(nb));
	va_end(ap);
	return (0);
}
