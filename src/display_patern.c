/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** display ring which grow
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"
#include "ring.h"

char	*yin_yang(void)
{
	char	*str = "ooooobbbbbb\nooobbbbbbbbbb\noobbbbbbbbbbbb\n"\
		"obbbbnnbbbbbnnb\n""obbbbnnbbbbbnnb\nbbbbbbbbbbbbnnnb\n"\
		"bbbbbbbbbbbnnnnb\n""bbbbbbbbbbnnnnnb\nbbbbbbbbbnnnnnnb\n"\
		"bbbbbnnnnnnnnnnb\n""bbbbnnnnnnnnnnnb\nobbbnnnnbbnnnnb\n"\
		"obbbnnnnbbnnnnb\n""oobbbnnnnnnnnb\nooobbbbnnnnbb\n"\
		"ooooobbbbbb\n";
	char	*patern = malloc(sizeof(char) * (my_strlen(str) + 1));
	int	count = 0;

	while (str[count] != 0){
		patern[count] = str[count];
		count++;
	}
	patern[count] = 0;
	return patern;
}

int	draw_white(framebuffer_t *fbuffer, sfVector2i pos, char c)
{
	sfVector2i tmp = {pos.x + 50, pos.y + 50};

	if (c == 'b')
		draw_rectangle(fbuffer, pos, tmp, sfWhite);
	return 50;
}

void	draw_patern(framebuffer_t *fbuffer)
{
	char	*patern = yin_yang();
	static	sfVector2i pos = {560, 140};
	sfVector2i tmp = pos;
	int	count = 0;

	draw_rectangle(fbuffer, (sfVector2i){0, 0},
			(sfVector2i){1920, 1080}, sfBlack);
	while (patern[count] != 0){
		tmp.x = pos.x;
		while (patern[count] != '\n' && patern[count] != 0){
			tmp.x += draw_white(fbuffer, tmp, patern[count]);
			count++;
		}
		tmp.y += 50;
		count++;
	}
	free(patern);
}
void	patern(sfRenderWindow *window, sfTexture *texture, sfSprite *sprite,
		framebuffer_t *fbuffer)
{
	draw_patern(fbuffer);
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

int	main_patern(void)
{
	void	(*function)(sfRenderWindow*, sfTexture*, sfSprite*,
				framebuffer_t*);

	function = patern;
	open_window(function);
	return 0;
}
