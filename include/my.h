/*
** EPITECH PROJECT, 2017
** Librairy header
** File description:
** Librairy header
*/

#include <stdarg.h>

#ifndef MY_
#define MY_
/* Basic Librairy */
int	my_putchar(char);
int	my_putstr(char*);
int	my_putnbr(int);
int	my_strcmp(char*, char*);
int	my_strlen(char const*);
int	my_getnbr(char*);
long	my_compute_power_it(long, long);
char	*my_revstr(char*);

/* Additional Librairy */
int     my_printf(char*, ...);
char	*long_to_str(long);

/* Convert decimal in other bases*/
char    *convert_to_exa_low(int);
char    *convert_to_exa_up(int);
char    *convert_to_octal(int);
char    *convert_to_binary(int);

/* Flags for my_printf */
int	my_putchar_p(va_list);
int	my_putstr_p(va_list);
int	my_putnbr_p(va_list);
int	bin_flag(va_list);
int	oct_flag(va_list);
int	dec_flag(va_list);
int	hexa_low_flag(va_list);
int	hexa_up_flag(va_list);
int	special_flag(va_list);
int	put_addres(va_list);

/*Others Functions for my_printf*/
char	*clear_zero(char*);
char    *full_res(char*);

#endif /* MY_ */
