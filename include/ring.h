/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** structure for ring
*/

#ifndef RING_H_
#define RING_H_

#include <SFML/Graphics.h>

struct ring {
	sfVector2i position;
	sfColor	color;
	sfColor	color_print;
	int	rayon;
	struct ball *next;
};
typedef struct ring ring_t;

#endif
