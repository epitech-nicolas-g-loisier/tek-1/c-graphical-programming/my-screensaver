/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** display multiple ball
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include <string.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"
#include "ball.h"

void	ball(sfRenderWindow *window, sfTexture *texture, sfSprite *sprite,
		framebuffer_t *fbuffer)
{
	static ball_t	*ball;
	ball_t	*tmp_ball = ball;
	int	count = 0;

	memset(fbuffer->pixels, 0, 8294400);
	if (ball == NULL){
		ball = first_ball();
		tmp_ball = ball;
	} else if (rand() % 20 == 0){
		ball = create_ball(ball);
		tmp_ball = ball;
	} while (tmp_ball != NULL){
		display_ball(fbuffer, tmp_ball);
		tmp_ball = tmp_ball->next;
		count++;
	} if (count >= 6)
		delete_ball(ball);
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

int	main_ball(void)
{
	void	(*function)(sfRenderWindow*, sfTexture*, sfSprite*,
				framebuffer_t*);

	function = ball;
	open_window(function);
	return 0;
}
