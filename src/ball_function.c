/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** Function for ball
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"
#include "ball.h"

ball_t	*first_ball(void)
{
	ball_t	*ball = malloc(sizeof(ball_t));
	int	rand_dir = rand() % 3;

	ball->position = (sfVector2i){rand() % 1000 + 460,rand() % 500 + 290};
	ball->speed = (sfVector2i){rand() % 10 + 10,rand() % 10 + 10};
	ball->color = (sfColor){rand() % 200 + 55, rand() % 200 + 55,
				rand() % 200 + 55, 255};
	ball->ray = rand() % 40 + 20;
	ball->next = NULL;
	if (rand_dir == 0)
		ball->speed.x = ball->speed.x * -1;
	else if (rand_dir == 1)
		ball->speed.y = ball->speed.y * -1;
	return (ball);
}

ball_t	*create_ball(ball_t *last_ball)
{
	ball_t	*ball = malloc(sizeof(ball_t));
	int	rand_dir = rand() % 3;

	ball->position = (sfVector2i){rand() % 1000 + 460, rand() % 500 + 290};
	ball->speed = (sfVector2i){rand() % 10 + 10, rand() % 10 + 10};
	ball->color = (sfColor){rand() % 200 + 55, rand() % 200 + 55,
				rand() % 200 + 55, 255};
	ball->ray = rand() % 40 + 20;
	ball->next = last_ball;
	if (rand_dir == 0)
		ball->speed.x = ball->speed.x * -1;
	else if (rand_dir == 1)
		ball->speed.y = ball->speed.y * -1;
	return (ball);
}

void	display_ball(framebuffer_t *fbuffer, ball_t *ball)
{
	draw_circle(fbuffer, ball->position, ball->ray, ball->color);
	ball->position.x += ball->speed.x;
	ball->position.y += ball->speed.y;
	if (ball->position.x >= (1920 - ball->ray - 5) ||
		ball->position.x <= (0 + ball->ray + 5))
		ball->speed.x = ball->speed.x * -1;
	if (ball->position.y >= (1080 - ball->ray - 5) ||
		ball->position.y <= (0 + ball->ray + 5))
		ball->speed.y = ball->speed.y * -1;
}

void	reduce_color(ball_t *tmp)
{
	ball_t	*tmp2;

	if (tmp->color.r >= 10)
		tmp->color.r = tmp->color.r - 10;
	if (tmp->color.r < 10 && tmp->color.g < 10 &&
		tmp->color.b < 10){
		tmp2 = tmp;
		free(tmp->next);
		if (tmp2->next == NULL){
			tmp2->next = NULL;
		} else
			tmp2->next = tmp2->next->next;
	}
}

void	delete_ball(ball_t* ball)
{
	ball_t	*tmp = ball;
	int	count = 0;

	while (count != 4){
		tmp = tmp->next;
		count++;
	} while (tmp != NULL){
		if (tmp->color.g >= 10)
			tmp->color.g = tmp->color.g - 10;
		if (tmp->color.b >= 10)
			tmp->color.b = tmp->color.b - 10;
		reduce_color(tmp);
		tmp = tmp->next;
	}
}
