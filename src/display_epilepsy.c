/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** give epilepsy
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"

void	flash(framebuffer_t *fbuffer)
{
	int	count = rand() % 3;

	while (count < (1920 * 1080 * 32 / 8)){
		if (fbuffer->pixels[count] != 0)
			fbuffer->pixels[count] -= 100;
		count += 4;
	}
}

void	epilepsy(sfRenderWindow *window, sfTexture *texture, sfSprite *sprite,
			framebuffer_t *fbuffer)
{
	unsigned int	rand_r = rand() % 50 + 20;
	unsigned int	rand_x = rand() % (1920 - rand_r * 2) + rand_r;
	unsigned int	rand_y = rand() % (1080 - rand_r * 2) + rand_r;
	sfUint8		red = rand() % 255;
	sfUint8		green = rand() % 255;
	sfUint8		blue = rand() % 255;
	sfColor		color = {red, green, blue, 255};
	static int	clock = 0;

	draw_circle(fbuffer, (sfVector2i){rand_x, rand_y}, rand_r, color);
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
	flash(fbuffer);
	clock++;
	if (clock == 600){
		draw_rectangle(fbuffer, (sfVector2i){0, 0},
				(sfVector2i){1920, 1080}, sfBlack);
		clock = 0;
	}
}

int	main_epilepsy(void)
{
	void	(*function)(sfRenderWindow*, sfTexture*, sfSprite*,
				framebuffer_t*);

	function = epilepsy;
	open_window(function);
	return 0;
}
