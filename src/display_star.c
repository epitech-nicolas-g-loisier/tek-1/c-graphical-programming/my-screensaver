/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** give epilepsy
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"

void	less_opacity(framebuffer_t *fbuffer)
{
	int	count = 0;

	while (count < (1920 * 1080 * 32 / 8)){
		if (fbuffer->pixels[count] < 30){
			fbuffer->pixels[count] = 0;
			fbuffer->pixels[count + 1] = 0;
			fbuffer->pixels[count + 2] = 0;
		} else {
			fbuffer->pixels[count] -= 5;
			fbuffer->pixels[count + 1] -= 5;
			fbuffer->pixels[count + 2] -= 5;
		}
		count += 4;
	}
}

void	star(sfRenderWindow *window, sfTexture *texture, sfSprite *sprite,
		framebuffer_t *fbuffer)
{
	unsigned int	rand_r = rand() % 50 + 10;
	unsigned int	rand_x = rand() % (1920 - rand_r * 3) + rand_r;
	unsigned int	rand_y = rand() % (1080 - rand_r * 3) + rand_r;
	unsigned int	rand_c = rand() % 55 + 200;
	sfColor		color = {rand_c, rand_c, rand_c, 255};

	draw_circle(fbuffer, (sfVector2i){rand_x, rand_y}, rand_r, color);
	less_opacity(fbuffer);
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

int	main_star(void)
{
	void	(*function)(sfRenderWindow*, sfTexture*, sfSprite*,
				framebuffer_t*);

	function = star;
	open_window(function);
	return 0;
}
