/*
** EPITECH PROJECT, 2017
** emacs
** File description:
** my_put_nbr.c
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

int	my_putnbr_p(va_list ap)
{
	int	nb = va_arg(ap, int);
	int	i = 0;
	char	*nbr = malloc(sizeof(char) * 13);

	nbr = full_res(nbr);
	if (nb < 0){
		nb = -nb;
		my_putchar('-');
	} while (nb != 0){
		nbr[i] = (nb % 10) + 48;
		nb = nb / 10;
		i++;
	}
	nbr = clear_zero(my_revstr(nbr));
	my_putstr(nbr);
	return (0);
}
