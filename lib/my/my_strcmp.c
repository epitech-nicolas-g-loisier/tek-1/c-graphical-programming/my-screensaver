/*
** EPITECH PROJECT, 2017
** my_strcmp
** File description:
** compare two strings
*/

int my_strcmp(char const *s1, char const *s2)
{
	int i = 0;
	int same = 0;
	int ascii;

	while (s1[i] != '\0' && s2[i] != '\0'){
		if (s1[i] == s2[i]){
			same++;
		} else if (s1[i] < s2[i]){
			ascii = s1[i] - s2[i];
			return (ascii);
		} else {
			ascii = s1[i] - s2[i];
			return (ascii);
		}
		i++;
	}
	if (s1[i] < s2[i]){
		ascii = s1[i] - s2[i];
		return (ascii);
	} else if (s1[i] > s2[i]){
		ascii = s1[i] - s2[i];
		return (ascii);
	}
	return (0);
}
