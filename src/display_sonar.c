/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** display ring which grow
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"
#include "ring.h"

ring_t	*first_sonar(void)
{
	ring_t	*ring = malloc(sizeof(ring_t));

	ring->position = (sfVector2i){960, 540};
	ring->color = (sfColor){rand() % 100 + 155, rand() % 100 + 155,
					rand() % 100 + 155, 255};;
	ring->rayon = 50;
	ring->next = NULL;
	return (ring);
}

void	display_sonar(framebuffer_t *fbuffer, ring_t *ring)
{
	draw_ring(fbuffer, ring->position, ring->rayon, ring->color);
	ring->rayon += 20;
	ring->color.r -= ring->color.r * 10 / 100 - 50;
	ring->color.g -= ring->color.g * 10 / 100 - 50;
	ring->color.b -= ring->color.b * 10 / 100 - 50;
}

void	display_reverse_sonar(framebuffer_t *fbuffer, ring_t *ring)
{
	ring->color.r = 0;
	ring->color.g = 0;
	ring->color.b = 0;
	draw_ring(fbuffer, ring->position, ring->rayon, ring->color);
	ring->rayon -= 20;
}

void	sonar(sfRenderWindow *window, sfTexture *texture, sfSprite *sprite,
		framebuffer_t *fbuffer)
{
	static	ring_t	*ring;
	static	int	max = 0;

	if (ring == NULL){
		ring = first_sonar();
	} else if (ring->rayon <= 50){
		max = 0;
		ring = first_sonar();
	} if (ring->rayon > 1200 || max == 1){
		display_reverse_sonar(fbuffer, ring);
		max = 1;
	} else if (ring->rayon < 1200 && max == 0){
		display_sonar(fbuffer, ring);
	}
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

int	main_sonar(void)
{
	void	(*function)(sfRenderWindow*, sfTexture*, sfSprite*,
				framebuffer_t*);

	function = sonar;
	open_window(function);
	return 0;
}
