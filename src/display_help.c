/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** -h of my_screensaver
*/

#include "my.h"

int	display_help(void)
{
	my_printf("animation rendering in a CSFML window.\n\n");
	my_printf("USAGE\n");
	my_printf("  ./my_screensaver [OPTIONS] animation_id\n");
	my_printf("  animation_id ID of the animation to process ");
	my_printf("(between 1 and 9).\n\n");
	my_printf("OPTIONS\n");
	my_printf("  -d print the description of all the ");
	my_printf("animations and quit.\n");
	my_printf("  -h print the usage and quit.\n");
	my_printf("\nUSER INTERACTIONS\n");
	my_printf("  MOUSE_MOVE		close the animation\n");
	my_printf("  MOUSE_CLICK		close the animation\n");
	my_printf("  KEY_PRESSED		close the animation\n");
	return (0);
}
