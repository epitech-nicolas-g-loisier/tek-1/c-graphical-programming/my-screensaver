/*
** EPITECH PROJECT, 2017
** emacqs
** File description:
** my_revstr.c
*/

char	*my_revstr(char *str)
{
	int	count = 0;
	int	rev_count = 0;
	char	guard;

	while (str[count + 1] != '\0')
		count = count + 1;
	while (count / 2 >= rev_count){
		guard = str[count - rev_count];
		str[count - rev_count] = str[rev_count];
		str[rev_count] = guard;
		rev_count = rev_count + 1;
	}
	return (str);
}
