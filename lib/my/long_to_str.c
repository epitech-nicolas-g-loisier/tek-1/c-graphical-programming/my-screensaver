/*
** EPITECH PROJECT, 2017
** Lib
** File description:
** convert lont to char*
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

char	*long_to_str(long nb)
{
	char	*nbr = malloc(sizeof(char) * 100);
	int	i = 0;

	while (nb != 0){
		nbr[i] = (nb % 10) + 48;
		nb = nb / 10;
		i++;
	}
	nbr[i] = 0;
	nbr = my_revstr(nbr);
	return (nbr);
}
