/*
** EPITECH PROJECT, 2017
** lib
** File description:
** clear zero before a number
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

char	*clear_zero(char *str)
{
	int	i = 0;
	int	j = 0;
	char	*new_str;

	while (str[i] == '0'){
		i++;
	}
	new_str = malloc(sizeof(char) * (my_strlen(&str[i]) + 1));
	new_str[j] = '0';
	while (str[i] != 0){
		new_str[j] = str[i];
		i++;
		j++;
	}
	return (new_str);
}

char	*full_res(char *result)
{
	int	i = 0;

	while (i <= 1){
		result[i] = '0';
		i++;
	}
	result[i] = 0;
	return (result);
}
