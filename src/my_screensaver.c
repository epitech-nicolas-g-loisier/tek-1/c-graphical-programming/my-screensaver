/*
** EPITECH PROJECT, 2017
** My screen saver
** File description:
** make screensaver in CSFML
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "my_screensaver.h"

void	display_error(char *arg)
{
	write(2, "./my_screensaver.html: bad arguments: ", 38);
	write(2, arg, my_strlen(arg));
	write(2, " given but require 1.\n", 22);
	write(2, "retry with -h\n", 14);
}

int	check_arg(char *arg)
{
	int	check = 0;
	char	*str[11] = {"-h", "-d", "1", "2", "3", "4", "5", "6", "7", "8",
				"9"};

	while (check != 11){
		if (my_strcmp(str[check], arg) == 0)
			return (check);
		check++;
	}
	return (84);
}

int	main(int argv, char **argc)
{
	int	check = 0;
	int	(*use_function[12])(void);

	use_function[0] = &display_help;
	use_function[1] = &display_id;
	use_function[2] = &main_draw_line;
	use_function[3] = &main_epilepsy;
	use_function[4] = &main_star;
	use_function[5] = &main_root;
	use_function[6] = &main_beat;
	use_function[7] = &main_ball;
	use_function[8] = &main_eye;
	use_function[9] = &main_sonar;
	use_function[10] = &main_patern;
	if (argv != 2){
		display_error("0");
		return (84);
	}
	check = check_arg(argc[1]);
	if (check == 84){
		display_error(argc[1]);
		return (84);
	}
	use_function[check]();
	return (0);
}
