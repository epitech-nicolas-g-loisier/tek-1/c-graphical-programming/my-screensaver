/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** display ID and description of screensaver
*/

#include "my.h"

int	display_id(void)
{
	my_printf("1:  Line: Random horizontal or vertical line\n");
	my_printf("2:  Epilepsy: \n");
	my_printf("3:  Stars:\n");
	my_printf("4:  Root: \n");
	my_printf("5:  Beat of color: \n");
	my_printf("6:  Pong ball:  \n");
	my_printf("7:  Eye: \n");
	my_printf("8:  Hypnosis: \n");
	my_printf("9:  Yin-Yang\n");
	return (0);
}
