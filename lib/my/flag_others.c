/*
** EPITECH PROJECT, 2017
** lib for my_printf
** File description:
** flags for my_printf
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

int	special_flag(va_list ap)
{
	int	i = 0;
	char	*str = va_arg(ap, char*);

	while (str[i] != 0){
		if (str[i] < 32 || str[i] >= 127){
			my_putchar('\\');
			my_putstr(convert_to_octal(str[i]));
		} else
			my_putchar(str[i]);
		i++;
	}
	return (0);
}

int	put_addres(va_list ap)
{
	long	nb = va_arg(ap, long);

	if (nb == 0)
		write(2, "(nil)", 5);
	else {
		my_putstr("0x");
		my_putstr(convert_to_exa_low(nb));
	}
	return (0);
}
