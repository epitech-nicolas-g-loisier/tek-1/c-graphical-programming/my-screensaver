/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** give epilepsy
*/

#include <SFML/Graphics.h>
#include <stdlib.h>
#include "my.h"
#include "my_screensaver.h"
#include "framebuffer.h"

sfColor	calc_new_color(sfColor color, int *more, int add)
{
	if (add == 1){
		color.r += more[0];
		color.g += more[1];
		color.b += more[2];
	} else {
		color.r -= more[0];
		color.g -= more[1];
		color.b -= more[2];
	}
	return color;
}
void	flash_desktop(framebuffer_t *fbuffer)
{
	static	int	count = 1;
	static	int	add = 1;
	static	sfColor	color = {255, 125, 0, 255};
	int	more[3] = {color.r / 40, color.g / 40, color.b / 40};
	static	sfColor	new_color = {0, 0, 0, 255};

	draw_rectangle(fbuffer, (sfVector2i){0, 0}, (sfVector2i){1920, 1080},
			new_color);
	new_color = calc_new_color(new_color, more, add);
	count += add;
	if (count == 39)
		add = -1;
	else if (count == 1){
		new_color = (sfColor){0, 0, 0, 255};
		color.r = rand() % 155 + 100;
		color.g = rand() % 155 + 100;
		color.b = rand() % 155 + 100;
		add = 1;
	}
}

sfVector2i	beat_display(sfVector2i pos, framebuffer_t *fbuffer)
{
	static int r = 10;
	static int check = 0;
	static sfColor color = {125, 56, 205, 255};

	flash_desktop(fbuffer);
	draw_circle(fbuffer, pos, r, color);
	draw_ring(fbuffer, pos, r, sfWhite);
	if (check == 1)
		r -= 5;
	else if (r < 200)
		r += 5;
	if (r == 200)
		check = 1;
	else if (r == 10){
		check = 0;
		color = (sfColor){rand() % 255, rand() % 255, rand() % 255,
					255};
	}
	return (pos);
}

void	beating(sfRenderWindow *window, sfTexture *texture, sfSprite *sprite,
			framebuffer_t *fbuffer)
{
	static sfVector2i pos = {960, 540};
	static int count = 0;

	draw_rectangle(fbuffer, (sfVector2i){0, 0},
			(sfVector2i){1920, 1080}, sfBlack);
	pos = beat_display(pos, fbuffer);
	count++;
	if (count == 76){
		pos = (sfVector2i){rand() % 960 + 200, rand() % 540 + 200};
		count = 0;
	}
	sfTexture_updateFromPixels(texture, fbuffer->pixels, 1920, 1080, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

int	main_beat(void)
{
	void	(*function)(sfRenderWindow*, sfTexture*, sfSprite*,
				framebuffer_t*);

	function = beating;
	open_window(function);
	return 0;
}
