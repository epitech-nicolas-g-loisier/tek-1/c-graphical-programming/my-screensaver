/*
** EPITECH PROJECT, 2017
** My Screen Saver
** File description:
** Header for my_screensaver
*/

#include <SFML/Graphics.h>
#include "framebuffer.h"

#ifndef MY_SCREENSAVER_
#define MY_SCREENSAVER_

int	display_help(void);
int	display_id(void);
int	main_draw_line(void);
int	main_epilepsy(void);
int	main_star(void);
int	main_root(void);
int	main_beat(void);
int	main_ball(void);
int	main_eye(void);
int	main_sonar(void);
int	main_patern(void);
int	open_window(void (*function)(sfRenderWindow*, sfTexture*, sfSprite*,
					framebuffer_t*));

#endif /* MY_SCREENSAVER_ */
